package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (rot13 rot13Reader) Read(b []byte) (int, error) {
	i, err := rot13.r.Read(b)
	if err == io.EOF {
		return 0, io.EOF
	}
	for j, z := range b {
		if j >= i {
			break
		}
		switch {
		case z >= 'A' && z <= 'M' || z >= 'a' && z <= 'm':
			b[j] = z + 13
		case z >= 'N' && z <= 'Z' || z >= 'n' && z <= 'z':
			b[j] = z - 13
		}
	}
	return i, err
}
func main() {
	// s := strings.NewReader("Lbh penpxrq gur pbqr!")
	s := strings.NewReader("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
	fmt.Println("NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
