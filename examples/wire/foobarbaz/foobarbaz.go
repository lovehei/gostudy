package foobarbaz

import (
	"context"
	"errors"

	"github.com/google/wire"
)

// Foo foo
type Foo struct {
	X int
}

// ProvideFoo return a Foo
func ProvideFoo() Foo {
	return Foo{X: 42}
}

// Bar bar
type Bar struct {
	X int
}

// ProvideBar return a Bar: a negative Foo
func ProvideBar(foo Foo) Bar {
	return Bar{X: -foo.X}
}

// Baz baz
type Baz struct {
	X int
}

// ProvideBaz return a value if Bar is not zero
func ProvideBaz(ctx context.Context, bar Bar) (Baz, error) {
	if bar.X == 0 {
		return Baz{}, errors.New("cannot provide baz when bar is zero")
	}
	return Baz{X: bar.X}, nil
}

// SuperSet superset
var SuperSet = wire.NewSet(ProvideFoo, ProvideBar, ProvideBaz)

// MegaSet megaset
var MegaSet = wire.NewSet(SuperSet)
