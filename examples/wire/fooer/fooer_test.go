package fooer

import (
	"testing"
)

func TestFooer(t *testing.T) {
	if "Hello, World!" != injectFooer().Foo() {
		t.Errorf("want Hello, World!, but get %s", injectFooer().Foo())
	}

	t.Fatalf("success")
}
