package fooer

import (
	"fmt"

	"github.com/google/wire"
)

// Fooer fooer
type Fooer interface {
	Foo() string
}

// MyFooer a implement Fooer
type MyFooer string

// Foo a fooer
func (b *MyFooer) Foo() string {
	return string(*b)
}

func provideMyFooer() *MyFooer {
	b := new(MyFooer)
	*b = "Hello, World!"
	return b
}

// Bar bar
type Bar string

func provideBar(f Fooer) Bar {
	fmt.Println("bar")
	return Bar(f.Foo())
}

// Set set
var Set = wire.NewSet(
	provideMyFooer,
	wire.Bind(new(Fooer), new(*MyFooer)),
	provideBar,
)
