package main

import "fmt"

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("hello 虽然都错了我还可以继续", err)
		}
	}()
	go f1()
	fmt.Println("main")
}

func f1() {
	defer func() {
		fmt.Println("f1 defer")
	}()
	f2()
}

func f2() {
	defer func() {
		fmt.Println("f2 defer")
	}()
	panic("f2 panic")
}
