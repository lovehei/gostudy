package main

import (
	"fmt"
	"os"
	"syscall"
)

func main() {
	filename := "haha.txt"
	for try := 0; try < 2; try++ {
		file, err := os.Create(filename)
		if err == nil {
			return
		}
		if e, ok := err.(*os.PathError); ok && e.Err == syscall.ENOSPC {
			fmt.Println("删除磁盘临时文件") // 恢复一些空间。
			continue
		}
		fmt.Println(file.Name())
	}
}
