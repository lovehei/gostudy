package main

import (
	"fmt"

	"golang.org/x/tour/tree"
)

// Walk 步进 tree t 将所有的值从 tree 发送到 channel ch。
func Walk(t *tree.Tree, ch chan int) {
	if t == nil {
		return
	}

	Walk(t.Left, ch)
	ch <- t.Value
	Walk(t.Right, ch)
}

// Same 检测树 t1 和 t2 是否含有相同的值。
func Same(t1, t2 *tree.Tree) bool {
	t1Ch := make(chan int, 10)
	t2Ch := make(chan int, 10)
	go Walk(t1, t1Ch)
	go Walk(t2, t2Ch)

	for i := 0; i < 10; i++ {
		if <-t1Ch != <-t2Ch {
			return false
		}

	}
	return true
}

func main() {
	t := tree.New(5)
	ch := make(chan int, 10)
	go Walk(t, ch)
	for i := 0; i < 10; i++ {
		fmt.Println(<-ch)
	}

	fmt.Printf("是不是相同的树：%t", Same(tree.New(1), tree.New(1)))
}
