// Package named 命名规则
package named

import "fmt"

// People define a People struct
// 定义一个人的类型
type People struct {
	name string
	Sex  int
}

// Name get the name of people
// 获取人的姓名
func (p People) Name() string {
	return p.name
}

// SetName get the name of people
// 设置人的姓名
func (p People) SetName(name string) error {
	if len(name) > 5 {
		return fmt.Errorf("名字不能大于5个汉字！")
	}
	p.name = name
	return nil
}
