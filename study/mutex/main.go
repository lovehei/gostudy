package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

var a, b int64

func main() {
	done := make(chan bool, 1)
	var mu sync.Mutex
	go func() {
		for {
			select {
			case <-done:
				return
			default:
				mu.Lock()
				time.Sleep(100 * time.Microsecond)
				atomic.AddInt64(&a, 1)
				mu.Unlock()
			}
		}
	}()

	for i := 0; i < 100000; i++ {
		atomic.AddInt64(&b, 1)
		mu.Lock()
		mu.Unlock()
	}
	done <- true
	fmt.Printf("a = %d b = %d", a, b)
}
