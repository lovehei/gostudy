module gitee.com/lovehei/gostudy

go 1.15

require (
	github.com/facebook/ent v0.5.3
	github.com/go-sql-driver/mysql v1.5.1-0.20200311113236-681ffa848bae
	github.com/golang/protobuf v1.4.3
	github.com/google/wire v0.4.0
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	golang.org/x/tour v0.0.0-20201106234542-4bb7d986198d
	google.golang.org/genproto v0.0.0-20201214200347-8c77b98c765d
	google.golang.org/protobuf v1.25.0
)
