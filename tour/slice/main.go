package main

import "fmt"

func main() {
	s := make([]int, 1)

	fmt.Printf("slice len = %d, cap = %d \n", len(s), cap(s))

	s = append(s, 1)
	fmt.Printf("slice len = %d, cap = %d \n", len(s), cap(s))

	s = append(s, 1)
	fmt.Printf("slice len = %d, cap = %d \n", len(s), cap(s))

	s = append(s, 1)
	s = append(s, 1)
	fmt.Printf("slice len = %d, cap = %d \n", len(s), cap(s))
}
