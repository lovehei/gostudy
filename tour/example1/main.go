package main

import "fmt"

func main() {
	x := 7.0
	z := x
	pre := 0.0
	for i := 1; i < 100; i++ {
		z -= (z*z - x) / (2 * z)
		if pre == z {
			fmt.Println("一共执行", i, "次")
			break
		}
		pre = z
		fmt.Println("z =", z)
	}
}
