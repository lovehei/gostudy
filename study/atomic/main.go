package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// Config config
type Config struct {
	a []int
}

func main() {
	var v atomic.Value
	v.Store(&Config{})
	go func() {
		i := 0
		for {
			i++
			cfg := &Config{a: []int{i, i + 1, i + 2, i + 3, i + 4, i + 5}}
			v.Store(cfg)
		}
	}()

	var wg sync.WaitGroup
	for n := 0; n < 4; n++ {
		wg.Add(1)
		go func() {
			for m := 0; m < 100; m++ {
				fmt.Printf("%v\n", v.Load())
			}
			wg.Done()
		}()
	}

	wg.Wait()
}
