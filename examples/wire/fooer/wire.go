//+build wireinject

package fooer

import "github.com/google/wire"

func injectFooer() Fooer {
	wire.Build(Set)
	return nil
}
