// Package semicolon 分号的规则
// 如果换行前的最后一个标记是标识符 go 的内建类型 (int float64 string)
// break continue fallthrough return ++ -- ) }
// 如果新行前的标记为语句的末尾，则插入分号
package semicolon

import "fmt"

func semicolon() {
	var number int
	number = 45
	fmt.Print(number)
}
