package main

import (
	"errors"
	"fmt"
	"os"
	"time"
)

// Message message
type Message string

// NewMessage new a message
func NewMessage(phrase string) Message {
	return Message(phrase)
}

// Greeter greeter
type Greeter struct {
	Message Message
	Grumpy  bool
}

// NewGreeter new greeter
func NewGreeter(m Message) Greeter {
	var grumpy bool
	if time.Now().Unix()%2 == 0 {
		grumpy = true
	}
	return Greeter{Message: m, Grumpy: grumpy}
}

// Greet greet
func (g Greeter) Greet() Message {
	if g.Grumpy {
		return Message("Go away!")
	}
	return g.Message
}

// Event event
type Event struct {
	Greeter Greeter
}

// NewEvent new enent
func NewEvent(g Greeter) (Event, error) {
	if g.Grumpy {
		return Event{}, errors.New("could not create event:event greeter is grumpy")
	}
	return Event{Greeter: g}, nil
}

// Start start a enent
func (e *Event) Start() {
	msg := e.Greeter.Greet()
	fmt.Println(msg)
}

func main() {
	e, err := InitializeEvent("hi welcome!")
	if err != nil {
		fmt.Printf("failed to create event :%s\n", err)
		os.Exit(2)
	}
	e.Start()
}
