//+build wireinject

package main

import (
	"context"

	"gitee.com/lovehei/gostudy/examples/wire/foobarbaz"

	"github.com/google/wire"
)

// InitializeEvent InitializeEvent
func InitializeEvent(phrase string) (Event, error) {
	wire.Build(NewEvent, NewGreeter, NewMessage)
	return Event{}, nil
}

/*  */
func initializeBaz(ctx context.Context) (foobarbaz.Baz, error) {
	wire.Build(foobarbaz.MegaSet)
	return foobarbaz.Baz{}, nil
}
