// Package control 是一个控制结构的展示包
package control

import (
	"fmt"
	"os"
)

// Min 获取两个整数中较小的值
func Min(a, b int) int {
	if a >= b {
		return b
	}
	return a
}

// if 判断中
func open(name string) error {
	var f *os.File
	var err error
	if f, err = os.Open(name); err != nil {
		return err
	}
	fmt.Fprint(f, "hello")
	f.Close()
	return nil
}

// for

func for100() {
	sum := 0
	for {
		i := 1
		if i > 100 {
			break
		}
		sum += i
	}
	fmt.Println(sum)

	for i := 1; i < 100; i++ {
		sum += i
	}
	fmt.Println(sum)

	var j int
	for j < 100 {
		sum += j
	}
	fmt.Println(sum)
}

func char() {
	for pos, char := range "日本\x80語" { // \x80 在 UTF-8 编码中是一个非法字符
		fmt.Printf("character %#U starts at byte position %d\n", char, pos)
	}
}

func unhex(c byte) byte {
	switch {
	case '0' <= c && c <= '9':
		return c - '0'
	case 'a' <= c && c <= 'f':
		return c - 'a' + 10
	case 'A' <= c && c <= 'F':
		return c - 'A' + 10
	}
	return 0
}
