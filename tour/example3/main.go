package main

import (
	"strings"

	"golang.org/x/tour/wc"
)

// WordCount WordCount
func WordCount(s string) map[string]int {
	words := strings.Split(s, " ")
	wordCount := make(map[string]int)
	for _, w := range words {
		wordCount[w]++
	}
	return wordCount
}

func main() {
	wc.Test(WordCount)
}
