//+build wireinject

package main

import (
	"gitee.com/lovehei/gostudy/examples/ent/ent"
	"github.com/google/wire"
)

type DB struct {
	Client *ent.Client
}

func NewDB() (*DB, error) {
	client, err := ent.Open("mysql", "root:123456@tcp(127.0.0.1:3306)/moive?parseTime=True")
	if err != nil {
		return nil, err
	}
	return &DB{Client: client}, nil
}

/*  */
func initializeDB() (*DB, error) {
	wire.Build(NewDB)
	return &DB{}, nil
}
