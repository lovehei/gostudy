package named

import (
	"fmt"
	"testing"
)

func TestPeopleName(t *testing.T) {
	p := People{Sex: 1}
	err := p.SetName("我果然是个天才!")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(p.Name())
}
