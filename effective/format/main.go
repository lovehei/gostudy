package main

import "fmt"

// 使用tab缩进
// 使用go fmt格式化代码
// 单行字符限制
func main() {
	fmt.Println("Hello Golang!")
	sum := 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 +
		12 + 13 + 14 + 15 + 16 + 17 + 18 + 19 + 20
	fmt.Println(sum)
}
